This project was created using PHP Slim Framework, NodeJS, Vanilla JS and jQuery.
The project is divided into two sections, Frontend and Backend.

## BackEnd
Backend was developed using slim framework and application is deployed using docker containers. Click to view [API documentation](https://documenter.getpostman.com/view/1109531/Szmjyurf?version=latest#39792c34-5dfa-4886-96e1-0a51a69bca99).


### `sh setup.sh`
This runs the shell scripts in the root directory and sets up the database, nginx server, phpmyadmin, database migrations, php and nodejs container. 
If I an error occurs while running script, you can follow this step by step in the root directory. 

**Run only if sh setup.sh fails.**
*  `docker-compose up -d` : Runs the docker-compose.yml in the root directory. 
*  `docker container ls` :  To get the php container name which will be used for the next command. 
*  `docker exec battlefield_php_1 sh -c "cd .. && composer install"`: Replace 'battlefield_php_1' with php container name and run command. Command installs composer dependencies.
*  `docker exec battlefield_php_1 sh -c "cd .. && php vendor/bin/phinx migrate -c bootstrap/config-phinx.php"` : Replace 'battlefield_php_1' with php container name and run command. Command initializes migrations and creates all needed table.
*  `docker exec battlefield_php_1 sh -c "cd .. && composer dump-autoload"` : Generates the list of all classes that need to be included in the project.
*  `docker exec battlefield_node_1 sh -c "npm start"` : Runs the node project.


Check server.<br />
Open [http://localhost:8088](http://localhost:8088) to view running server.

Check frontend app.<br />
Game is served in [http://localhost:3000/](http://localhost:3000/)

Check created db tables.<br />
Open [http://localhost:8004/](http://localhost:8004) to view phpmyadmin in browser.

Open server API documentation.<br />
Open [server api documentation](https://documenter.getpostman.com/view/1109531/Szmjyurf?version=latest#39792c34-5dfa-4886-96e1-0a51a69bca99) to view api documentation.


The API_URL can be changed in /frontend/public/assets/js/config.js



