<?php
/**
 * Created by PhpStorm.
 * User: ayobami
 * Date: 5/8/2020
 * Time: 10:20 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'games';

    protected $primaryKey = 'id';

    protected $fillable = ['gamecode', 'player_id','opponent_id','end'];

    public function generateCode()
    {
        $charset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        $base = strlen($charset);
        $result = '';
        $now = explode(' ', microtime())[1];
        while ($now >= $base) {
            $i = $now % $base;
            $result = $charset[$i] . $result;
            $now /= $base;
        }

        return substr($result, -7);
    }

    public function gameCanvas()
    {
        return $this->hasMany(GameCanvas::class, 'game_id');
    }
}