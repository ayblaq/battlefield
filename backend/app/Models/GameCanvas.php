<?php
/**
 * Created by PhpStorm.
 * User: ayobami
 * Date: 5/8/2020
 * Time: 10:20 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Valitron\Validator;

class GameCanvas extends Model
{
    protected $table = 'games_canvas';

    protected $primaryKey = 'id';

    protected $fillable = ['left', 'width', 'height', 'top',
        'player_id', 'status', 'ship_title', 'game_id'];


    public function validateInput($canvas,$location)
    {
        $result = $this->validateCanvas($canvas);
        if($result){
            return $this->validateLocation($location);
        }
        return $result;
    }

    public function validateCanvas($canvas){
        foreach ($canvas as $loc) {
            $validator = $this->validator($loc);
            if (!$validator->validate()) {
                return [$validator->errors()];
            }
        }

        return true;
    }

    public function validateLocation($location) {
        $ships = [];
        foreach($location as $key => $loc){
            $validator = $this->validator($loc);
            if (!$validator->validate()) {
                return [$validator->errors()];
            }
            array_push($ships,$key);
            foreach($location as $ship => $pos){
                if(in_array($ship,$ships))continue;
                if($loc['left']==$pos['left']&&$loc['top']==$pos['top']&&
                    $pos['width']==$loc['width']&&$pos['height']==$loc['height']){
                    return [
                        'Error' => $key.' and '.$ship.' have a position conflict'
                    ];
                }
            }
        }

        return true;
    }

    public function validator($input)
    {
        $validator = new Validator($input);
        $rules = [
            'top' => ['required', 'integer'],
            'left' => ['required', 'integer'],
            'height' => ['required', 'integer'],
            'width' => ['required', 'integer'],
        ];
        $validator->mapFieldsRules($rules);
        $validator->labels([
            'top' => 'top',
            'height' => 'height',
            'left' => 'left',
            'width' => 'width'
        ]);

        return $validator;
    }

    public function validatePos($input){
        $validator = new Validator($input);
        $rules = [
            'X' => ['required', 'integer'],
            'Y' => ['required', 'integer'],
        ];
        $validator->mapFieldsRules($rules);
        $validator->labels([
            'X' => 'width',
            'Y' => 'top',
        ]);

        return $validator;
    }

    public function randomPosition($canvas)
    {
        $numbers = range(1, 100);
        shuffle($numbers);
        $random = array_slice($numbers, 0, 10);
        $location = [];
        foreach ($random as $key => $value) {
            $loc = $canvas[$value];
            $ship = 'ship' . $key;
            $location[$ship] = $loc;
            $location[$ship]['status'] = '';
            $location[$ship]['key'] = $ship;
        }

        return $location;
    }

    public function getStatus($player_id,$game_id){
        $data_array = [
            ['player_Id','!=',$player_id],
            ['game_id',$game_id]
        ];
        $result = GameCanvas::select('top','left','height','width','ship_title','status')
            ->where($data_array)->get();
        $ships = [];
        $miss = [];
        $hit = 0;
        foreach($result as $key => $value){
            if($value->ship_title !== ''){
                $ships[$value->ship_title] = $value;
                $hit = $value->status=="hit"?$hit+1:$hit;
            }
            else if($value->status=="miss"){
                $miss[] = $value;
            }
        }

        return ['ships'=>$ships,'hit'=>$hit,'miss'=>$miss];
    }
}