<?php

namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Valitron\Validator;
use \Firebase\JWT\JWT;
use \Tuupola\Base62;

class Player extends Model
{
    protected $table = 'players';

    protected $primaryKey = 'id';

    protected $fillable = ['nickname'];

    public function games()
    {

        return $this->hasMany(Game::class, 'player_id');
    }

    public function getValidator($input)
    {
        $validator = new Validator($input);
        $validator->rule('required', 'nickname');
        $validator->rule('lengthBetween', 'nickname', 5, 50);
        $validator->labels([
            'nickname' => 'nickname'
        ]);

        return $validator;
    }

    public function getPlayerToken($gameId)
    {
        $now = new DateTime("now");
        $expire = new DateTime("now +12 hours");
        $base62 = new Base62();
        $jti = $base62->encode(random_bytes(16));
        $secret = "iknowthisshouldntbecommited";
        $token = [
            "jti" => $jti,
            "iat" => $now->getTimestamp(),
            "exp" => $expire->getTimestamp(),
            "id" => $this->id,
            "gameId" => $gameId,
            "scope" => ["read", "write", "delete"]
        ];

        return JWT::encode($token, $secret, "HS256");
    }

    public function canvas()
    {
        return $this->hasMany(GameCanvas::class, 'player_id');
    }


}