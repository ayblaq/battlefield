<?php
namespace App\Controllers;

class BaseController
{
   protected $container;
   
   public function __construct($container){
       $this->container = $container;
   }
   public function __get($property){
       if($this->container->has($property)){
           return $this->container->get($property);
       }
       return $this->{$property};
   }
}
