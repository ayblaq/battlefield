<?php

namespace App\Controllers;

use App\Models\Game;
use App\Models\GameCanvas;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Http\Response as Response;
use App\Controllers\BaseController;
use App\Models\Player;

class PlayerCanvasController extends BaseController
{

    public function create(Request $request, Response $response)
    {
        $gamecode = $request->getAttribute('gameId');
        $game = Game::where('gamecode', $gamecode)->first();
        $playerId = $request->getAttribute('player_id');
        $body = $request->getParsedBody();
        $canvas = isset($body['canvas']) ? json_decode($body['canvas'], true) : [];
        $location = isset($body['location']) ? json_decode($body['location'], true) : [];
        $status = 200;
        if ($game->end == 1) {
            $data = [
                "Error" => 'Game already ended'
            ];
        }
        else if (count($canvas) < 100 || count($location) < 10) {
            $status = 403;
            $data = [
                'error' => 'Canvas Input should be 10 by 10'
            ];
        }
        else {

            $gamecanvas = new GameCanvas();
            $canvasValidator = $gamecanvas->validateInput($canvas,$location);
            if (is_bool($canvasValidator)) {
                $player = Player::find($playerId);
                if ($player->nickname == "K1") {
                    $status = 403;
                    $data = [
                        'error' => 'Canvas cannot be created for K1.'
                    ];
                } else {
                    $this->addPlayerCanvas($player, $game, $canvas, $location);
                    $comp = 'K1';
                    $k1 = Player::where('nickname', $comp)->first();
                    $k1Spot = $gamecanvas->randomPosition($canvas);
                    $this->addPlayerCanvas($k1, $game, $canvas, $k1Spot);
                    $data = [
                        'ships' => $k1Spot
                    ];
                }
            } else {
                $status = 403;
                $data = array_merge($canvasValidator);
            }
        }
        $oldResponse = $response->withHeader('Content-type', 'application/json');
        $newResponse = $oldResponse->withJson($data, $status);

        return $newResponse;

    }

    public function addPlayerCanvas(Player $player, Game $game, array $canvas, array $location)
    {
        $this->storeCanvas($canvas, $game, $player);
        $this->mapLocation($location, $game, $player);
    }

    public function storeCanvas(array $data, Game $game, Player $player)
    {
        $player->canvas()
            ->where(['game_id' => $game->id])
            ->delete();
        foreach ($data as $loc) {
            $data_array = [
                'height' => $loc['height'],
                'top' => $loc['top'],
                'width' => $loc['width'],
                'left' => $loc['left'],
                'game_id' => $game->id,
                'status' => '',
                'ship_title' => ''
            ];
            $player->canvas()
                ->create($data_array);
        }
    }

    public function mapLocation(array $data, Game $game, Player $player)
    {
        foreach ($data as $key => $loc) {
            $data_array = [
                'height' => $loc['height'],
                'top' => $loc['top'],
                'width' => $loc['width'],
                'left' => $loc['left'],
                'game_id' => $game->id,
            ];
            $player->canvas()
                ->where($data_array)
                ->update(['ship_title' => $key]);
        }
    }

    public function markpoint(Request $request, Response $response)
    {
        $oldResponse = $response->withHeader('Content-type', 'application/json');
        $gamecode = $request->getAttribute('gameId');
        $game = Game::where('gamecode', $gamecode)->first();
        $playerId = $request->getAttribute('player_id');
        $status = 200;
        if ($game->end == 1) {
            $data = [
                "Error" => 'Game already ended'
            ];
            $newResponse = $oldResponse->withJson($data, 403);
            return $newResponse;
        }
        $last = GameCanvas::where('game_id',$game->id)
                    ->latest("updated_at")->first();
        if($last->player_id!=$playerId && $last->status != ""){
            $data = [
                "Error" => 'Not your turn.'
            ];
            $newResponse = $oldResponse->withJson($data, 403);
            return $newResponse;
        }
        $body = $request->getParsedBody();
        $point = isset($body['point']) ? json_decode($body['point'], true) : [];
        $validator = GameCanvas::validatePos($point);
        $player = Player::find($playerId);
        if($player->nickname=="K1"){
            $point =  $this->selectRandom($player,$game);
        }
        else if(!$validator->validate()) {
            $data = $validator->errors();
            $newResponse = $oldResponse->withJson($data, 403);
            return $newResponse;
        }
        $result = $this->move($player, $point, $game);
        if(array_key_exists('error',$result)){
            $status=403;
        }
        $newResponse = $oldResponse->withJson($result, $status);

        return $newResponse;
    }

    public function move(Player $player, array $point, Game $game)
    {
        $result = [];
        $selection = GameCanvas::whereRaw($point['Y'] . " >= top AND " .
            $point['Y'] . " <= top + height AND " . $point['X'] . " >= games_canvas.left AND "
            . $point['X'] . " <= games_canvas.left + width AND " . $player->id
            . " != player_id AND " . $game->id . " = game_id AND status = '' ")->first();

        if ($selection) {
            if ($selection['ship_title'] != "") {
                GameCanvas::where(['id' => $selection->id])
                    ->update(['status' => 'hit']);
            } else {
                GameCanvas::where(['id' => $selection->id])
                    ->update(['status' => 'miss']);
            }
            $result = GameCanvas::getStatus($player->id, $game->id);
            $result['gameover'] = $result['hit'] > 9 ? true : false;
            $result['position'] = $selection;
            if ($result['hit'] > 9) {
                Game::where(['id' => $game->id])
                    ->update(['end' => 1]);
                $result['gameover'] = true;
            } else $result['gameover'] = false;
        }else {
            $result['error'] = "Invalid position clicked. Replay";
        }
        return $result;
    }

    public function selectRandom(Player $player, Game $game)
    {
        $data_array = [
            ['game_id', $game->id],
            ['player_id', '!=', $player->id],
            ['status', '']
        ];
        $not_found = true;
        $point = [];
        while ($not_found) {
            $result = GameCanvas::where($data_array)
                ->inRandomOrder()->first();
            if ($result->status == '') {
                $point = [
                    'X' => $result->left,
                    'Y' => $result->top
                ];
                $not_found = false;
            }
        }

        return $point;
    }
}