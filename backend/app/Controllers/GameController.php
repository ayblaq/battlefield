<?php

namespace App\Controllers;

use App\Models\Game;
use App\Models\GameCanvas;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Slim\Http\Response as Response;
use App\Controllers\BaseController;
use App\Models\Player;

class GameController extends BaseController
{


    public function create(Request $request, Response $response)
    {
        $body = $request->getParsedBody();
        $player = new Player();
        $oldResponse = $response->withHeader('Content-type', 'application/json');
        $validator = $player->getValidator($body);
        $status = 200;
        if ($validator->validate()) {
            $data = $this->store($body);
        } else {
            $data = $validator->errors();
            $status = 403;
        }
        $newResponse = $oldResponse->withJson($data, $status);

        return $newResponse;
    }

    public function gameturns(Request $request, Response $response){
        $route = $request->getAttribute('route');
        $args = $route->getArguments();
        $game  = Game::where('gamecode',$args['gameId'])->first();
        $oldResponse = $response->withHeader('Content-type', 'application/json');
        $status = 403;
        if($game){
            if($game->end == 1){
                $data['player'] = $this->playerTurn($game);
                $data['k1'] = $this->compTurn($game);
                $status = 200;
            }else {
                $data = ["Error" => "Game not yet over."];
            }
        }else {
            $data = ["Error" => "Game does not exist"];
        }

        $newResponse = $oldResponse->withJson($data, $status);
        return $newResponse;
    }

    public function playerTurn(Game $game) {
        $query = [['game_id', $game->id],['player_id', '!=', $game->player_id],['status', '!=', '']];

        return GameCanvas::where($query)->get(['height','left','width','status','top','ship_title']);
    }

    public function compTurn(Game $game) {
        $query = [['game_id', $game->id],['player_id', $game->player_id],['status', '!=', '']];

        return GameCanvas::where($query)->get(['height','left','width','status','top','ship_title']);
    }

    public function store($data)
    {
        $k1 = Player::firstorCreate([
            'nickname' => 'K1'
        ]);
        $player = Player::firstorCreate([
            'nickname' => $data["nickname"]
        ]);
        $game = new Game();
        $gamecode = $game->generateCode();
        $player->token = $player->getPlayerToken($gamecode);
        $k1->token = $k1->getPlayerToken($gamecode);
        $player->games()
            ->create([
                'player_id' => $player->id,
                'gamecode' => $gamecode
            ]);
        unset($player->id);
        unset($k1->id);

        return ['player' => $player, 'k1' => $k1, 'gamecode' => $gamecode];
    }
}