<?php

namespace App\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class SkipCors
{
    public function __invoke(Request $request, Response $response, $next)
    {
        // TODO: Implement __invoke() method.
        $response = $response->withHeader('Access-Control-Allow-Origin', '*');
        $response = $response->withHeader('Access-Control-Allow-Headers', 'Content-Type, Accept,Authorization');
        $response = $next($request, $response);
        return $response;

    }
}