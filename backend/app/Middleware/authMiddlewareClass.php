<?php

namespace App\Middleware;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class authMiddlewareClass
{
    public function __invoke(Request $request, Response $response, $next)
    {
        // TODO: Implement __invoke() method.
        $token = $request->getAttribute('token');
        $route = $request->getAttribute('route');
        $args = $route->getArguments();
        if ($token['gameId'] == $args['gameId']) {
            $request = $request->withAttribute('player_id', $token['id']);
            return $next($request, $response);
        } else {
            $oldResponse = $response->withHeader('Content-type', 'application/json');
            $data = ['error' => 'Game is not valid'];
            $newResponse = $oldResponse->withJson($data, 403);
            return $newResponse;
        }
    }
}