<?php

use Routes\GameRoute;

require '../bootstrap/app.php';
require '../bootstrap/container.php';
require '../bootstrap/config.php';
require '../bootstrap/config-phinx.php';

$app = (new GameRoute())->get();
$app->run();
