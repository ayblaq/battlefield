<?php

namespace Routes;

use App\Controllers\PlayerCanvasController;
use App\Controllers\GameController;
use App\Middleware\authMiddlewareClass;
use App\Middleware\SkipCors;
use \Psr\Http\Message\ResponseInterface as Response;
use \Tuupola\Middleware\JwtAuthentication as JWT;
//$app->add(new SkipCors());

class GameRoute
{
    private $app;

    public function __construct()
    {
        $this->app = new \Slim\App([
            'settings' => [
                'displayErrorDetails' => true,
                "determineRouteBeforeAppMiddleware" => true,
                'db' => [
                    'driver' => DB_ADAPTER,
                    'host' => DB_HOST,
                    'database' => DB_NAME,
                    'username' => DB_USER,
                    'password' => DB_PASS,
                    'pass' => DB_PASS,
                    'charset' => 'utf8',
                    'collation' => 'utf8_unicode_ci',
                ]
            ]
        ]);

        $this->app->add(new SkipCors());

        $this->app->add(new JWT([
            "secure" => false,
            "path" => "/api",
            "ignore" => ["/api/game", "/api/(.+)\/game"],
            "secret" => "iknowthisshouldntbecommited",
            "algorithm" => ["HS256"],
            "error" => function (Response $response, $arguments) {
                $data["status"] = "error";
                $data["message"] = $arguments["message"];
                $response = $response->withHeader('Access-Control-Allow-Origin', '*');
                return $response
                    ->withHeader("Content-Type", "application/json")
                    ->getBody()->write(json_encode($data), 401);
            }
        ]));

        $this->app->post('/api/game', GameController::class . ':create')->add(new SkipCors());

        $this->app->get('/api/{gameId}/game', GameController::class . ':gameturns')->add(new SkipCors());

        $this->app->post('/api/{gameId}/canvas', PlayerCanvasController::class . ':create')->add(new authMiddlewareClass())->add(new SkipCors());

        $this->app->post('/api/{gameId}/markpoint', PlayerCanvasController::class . ':markpoint')->add(new authMiddlewareClass())->add(new SkipCors());
    }

    public function get(){

        return $this->app;
    }
}