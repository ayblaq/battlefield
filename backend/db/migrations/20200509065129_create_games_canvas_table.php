<?php

use \Migrations\Migration;

class CreateGamesCanvasTable extends Migration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function up()
    {
        $this->schema->create('games_canvas', function (Illuminate\Database\Schema\Blueprint $table) {
            // Auto-increment id
            $table->bigIncrements('id');
            $table->unsignedBigInteger('left');
            $table->unsignedBigInteger('top');
            $table->unsignedBigInteger('height');
            $table->unsignedBigInteger('width');
            $table->unsignedBigInteger('player_id');
            $table->unsignedBigInteger('game_id');
            $table->string('ship_title');
            $table->enum('status', ['', 'hit','miss']);

            $table->foreign('player_id')
                ->references('id')
                ->on('players')
                ->cascadeOnDelete();

            $table->foreign('game_id')
                ->references('id')
                ->on('games')
                ->cascadeOnDelete();

            $table->timestamps();
        });
    }

    public function down()
    {
        $this->schema->drop('games_canvas');
    }
}
