<?php

namespace Tests\AuthTest;

use App\Models\Player;
use PHPUnit\Framework\TestCase;
use Routes\GameRoute;
use Slim\Http\Environment;
use Slim\Http\Request;

require '../../bootstrap/config.php';

class AuthTest extends TestCase{

    protected $app;
    protected $gameId;
    protected $token;

    protected function setUp() {
        $this->app = (new GameRoute())->get();
        $this->gameId = "jYgzK";
        $this->token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0V1VlZktCN2p6VVEzSVJ2N3huSnNEIiwiaWF0IjoxNTg5MTQ3NDI5LCJleHAiOjE1ODkxOTA2MjksImlkIjoxLCJnYW1lSWQiOiJqWHRxOSIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSIsImRlbGV0ZSJdfQ.xXTSNWdwYxxbamXWdB2QZYIrAfsRMZiP3lepBgXWfBY";
    }

    public function testEmptyAuthorization() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI' => '/api/'.$this->gameId.'/canvas',
            'HTTP_AUTHORIZATION'  => 'Bearer ',
        ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);

        $this->assertSame( 401,$response->getStatusCode());

        $result = $response->getBody();

        $this->assertSame('{"status":"error","message":"Token not found."}',(string)$result);

    }

    public function testInvalidAuthorization() {

        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/'.$this->gameId.'/canvas',
            'HTTP_AUTHORIZATION'  => 'Bearer '.$this->token,
        ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);

        $this->assertSame( 401,$response->getStatusCode());

        $result = $response->getBody();

        $this->assertSame('{"status":"error","message":"Expired token"}',(string)$result);
    }

    public function testInvalidGame() {

        $player = new Player();
        $player->id = 1;
        $this->token = $player->getPlayerToken("jYgzY");

        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/'.$this->gameId.'/canvas',
            'HTTP_AUTHORIZATION'  => 'Bearer '.$this->token,
        ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);

        $this->assertSame( 403,$response->getStatusCode());

        $result = $response->getBody();

        $this->assertSame('{"error":"Game is not valid"}',(string)$result);
    }
}