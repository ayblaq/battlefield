<?php

use App\Models\GameCanvas;
use http\Env\Response;
use PHPUnit\Framework\TestCase;
use Routes\GameRoute;
use Slim\Http\Environment;
use Slim\Http\Request;
require '../../bootstrap/config.php';

class GameCanvasTest extends TestCase {

    protected $app;

    protected function setUp() {
        $this->app = (new GameRoute())->get();
    }

    public function testShipLocation() {
        $gamecanvas = new GameCanvas();
        $testLocation = [
            'ship0' => ['left'=>180,'width'=>55,'top'=>420,'height'=>55],
            'ship1' => ['left'=>300,'width'=>55,'top'=>360,'height'=>55],
            'ship2' => ['left'=>420,'width'=>55,'top'=>360,'height'=>55],
            'ship3' => ['left'=>360,'width'=>55,'top'=>240,'height'=>55],
            'ship4' => ['left'=>480,'width'=>55,'top'=>240,'height'=>55]
        ];
        $result = $gamecanvas->validateLocation($testLocation);

        $this->assertTrue($result,true);
    }

    public function testShipOverlapping() {
        $gamecanvas = new GameCanvas();
        $testLocation = [
            'ship0' => ['left'=>180,'width'=>55,'top'=>420,'height'=>55],
            'ship1' => ['left'=>300,'width'=>55,'top'=>360,'height'=>55],
            'ship2' => ['left'=>420,'width'=>55,'top'=>360,'height'=>55],
            'ship3' => ['left'=>360,'width'=>55,'top'=>240,'height'=>55],
            'ship4' => ['left'=>360,'width'=>55,'top'=>240,'height'=>55]
        ];
        $result = $gamecanvas->validateLocation($testLocation);

        $result = json_encode($result);

        $this->assertEquals('{"Error":"ship3 and ship4 have a position conflict"}',$result);
    }

    public function testInvalidPosition() {
        $gamecanvas = new GameCanvas();
        $testLocation = [
            'ship0' => ['left'=>180,'width'=>55,'top'=>'','height'=>55],
            'ship1' => ['left'=>300,'width'=>55,'top'=>360,'height'=>55],
            'ship2' => ['left'=>420,'width'=>55,'top'=>360,'height'=>55],
            'ship3' => ['left'=>360,'width'=>55,'top'=>240,'height'=>55],
            'ship4' => ['left'=>360,'width'=>55,'top'=>240,'height'=>55]
        ];
        $result = $gamecanvas->validateLocation($testLocation);

        $result = json_encode($result);

        $this->assertEquals('[{"top":["top is required","top must be an integer"]}]',$result);
    }
}