<?php

namespace Tests\GameTest;

use http\Env\Response;
use PHPUnit\Framework\TestCase;
use Routes\GameRoute;
use Slim\Http\Environment;
use Slim\Http\Request;

require '../../bootstrap/config.php';

class GameTestCase extends TestCase {

    protected $app;

    protected function setUp() {
        $this->app = (new GameRoute())->get();
    }

    public function testGameEmptyNickname() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/game'
        ]);

        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);

        $this->assertSame(403, $response->getStatusCode());

        $expected = [
            'nickname' => [
                "nickname is required",
                "nickname must be between 5 and 50 characters"
            ]
        ];

        $this->assertSame((string)json_encode($expected),(string)$response->getBody());
    }

    public function testGameShortNickname() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/game',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);

        $expected = [
            'nickname' => [
                "nickname must be between 5 and 50 characters"
            ]
        ];

        $req = Request::createFromEnvironment($env)->withParsedBody(['nickname'=>'ay']);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);

        $this->assertSame( 403,$response->getStatusCode());

        $result = $response->getBody();

        $this->assertSame((string)json_encode($expected),(string)$result);

    }
}
