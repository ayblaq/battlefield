<?php
require __DIR__.'/config.php';

return [
    'paths' => [
        'migrations' => 'db/migrations'
    ],
    'migration_base_class' => '\Migrations\Migration',
    'environments' => [
        'default_database' => 'db',
        'db' => array(
            'adapter' => DB_ADAPTER,
            'host' => 'mysql',
            'name' => 'app_db',
            'user' => 'root',
            'pass' => 'root',
            'password' => 'root',
            'port' => DB_PORT
        )
    ]
];