<?php

use Routes\GameRoute;

$app = (new GameRoute())->get();
$container = $app->getContainer();

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function ($container) use ($capsule) {
    return $capsule;
};

/**
 * @param $container
 * @return \App\Controllers\GameController
 */
$container['PlayerController'] = function ($container) {
    return new \App\Controllers\GameController($container->get('settings'));
};

/**
 * @param $container
 * @return \App\Controllers\PlayerCanvasController
 */
$container['PlayerCanvasController'] = function ($container) {
    return new \App\Controllers\PlayerCanvasController($container->get('settings'));
};