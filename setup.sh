#!/bin/sh
docker-compose up -d
docker exec battlefield_php_1 sh -c "cd .. && rm -r vendor/"
docker exec battlefield_php_1 sh -c "cd .. && composer install"
docker exec battlefield_php_1 sh -c "cd .. && php vendor/bin/phinx migrate -c bootstrap/config-phinx.php"
docker exec battlefield_php_1 sh -c "cd .. && composer dump-autoload"