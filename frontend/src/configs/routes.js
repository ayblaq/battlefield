const {
    sendResponse,
    getContentType
  } = require( './communications.js' );
  function routes( request, response ) {
    switch ( true ) {
      case request.url === '/' || request.url === "":
        sendResponse( 'home/index.html', 'text/html', response );
        break;
      case request.url === '/game':
        sendResponse( 'arena/index.html', 'text/html', response );
        break;
      default:
        sendResponse( request.url, getContentType( request.url ), response );
    }
  }
  
  exports.handler = routes;