const port = '3000';
const hostname = '0.0.0.0';
const http = require( 'http' );
const routes = require( './src/configs/routes' );
http.createServer(routes.handler)
 .listen( port, hostname, () => {
   console.log( `Server is running at http://${ hostname }:${ port }/` );
 } );