import Grid from './grid.js'
let state = {
    grid: new Grid("#game-section", "#arena-container", 10, 10),
    clicked: false
}

$("#start-section").on("change paste keyup keydown", ".nick-input", function (event) {
    let word = $(this).val()
    $.trim(word).length > 4 ? $("#btn-start").attr("disabled", false) : $("#btn-start").attr("disabled", true)
    state.grid.data['player'].nickname = word
})

$("#start-section").on("click", "#btn-start", function (ev) {
    state.grid.toggleModal.show('Loading canvas', 'loading')
    startGame()
})

$("#game-section").on("click", "#btn-restart", function (ev) {
    let nickname = state.grid.data['player'].nickname
    state.grid = new Grid("#game-section", "#arena-container", 10, 10)
    state.grid.data['player'].nickname = nickname
    state.clicked = false
    state.grid.toggleModal.show('Loading canvas', 'loading')
    startGame()
    $('#game-status').addClass("hidden")
    $('#game-status').empty()
    $('#arena-container').empty()
    $(this).remove()
})

$("#game-section").on("click", "#btn-playgame", function (ev) {
    state.grid.toggleModal.show('Loading game arena', 'loading')
    state.grid.setCanvas(JSON.stringify(state.grid.elements), state.gameId)
        .then(response => {
            if (!response.ok) {
                throw response
            }
            return response.json()
        }).then(result => {
            state.grid.toggleModal.hide()
            state.grid.data['k1'].ships = result.ships
            $("#arena-container").empty()
            state.grid.setUpCanvas('player', 'player', true)
            state.grid.setUpCanvas('k1', 'k1', true)
            $("#player").addClass("hidden")
            state.grid.setUpGamestatus('#game-status', state.grid.data['player'].nickname)
        }).catch(error => {
            state.grid.showError(error)
        })
})

$("#arena-container").on("click", "canvas[id=k1]", function (ev) {
    let point = { 'X': ev.pageX, 'Y': ev.pageY }
    if (state.clicked || state.grid.gameover == true) {
        return
    }
    else {
        state.clicked = true
        let result = state.grid.gridShoot(point, "k1")
        if (result) {
            point = {'X':result['left'],'Y':result['top']}
            state.grid.updateStatus('player')
            state.grid.toggleModal.show("Saving your play", "loading")
            state.grid.markPoint('player', point, state.gameId)
                .then(result => {
                    state.clicked = result.clicked
                    if(!state.clicked)state.grid.toggleModal.show('It is your turn','Turn',true)
                }).catch(error => {
                    state.grid.showError(error)
                    state.clicked = state.grid.gameover?true:false
                })
        }
        else {
            state.clicked = false;
        }
    }
})

$("#game-status").on("click",".tag-cloud-canvas", function(ev){
    if(!this.classList.contains('active')){
        let player = this.getAttribute('data-canvas')
        let opponent = state.grid.getOpponent(player)
        $(`#${player}`).removeClass('hidden')
        $(`#${opponent}`).addClass('hidden')
        $("#game-status").find('.active').removeClass('active')
        this.classList.add('active')
    }
})

let startGame = () => {

    state.grid.getGameId(state.grid.data['player'].nickname)
    .then(response => {
        if (!response.ok) throw response
        return response.json()
    }).then(result => {
        state.grid.toggleModal.hide()
        $("#start-section").addClass("hidden")
        $("#arena-container").empty()
        state.gameId = result.gamecode
        state.grid.data['k1'] = { ...result['k1'], ...state.grid.data['k1'] }
        state.grid.data['player'] = { ...result['player'], ...state.grid.data['player'] }
        state.grid.setUpCanvas('canvas_player', 'player')
    }).catch(error => {
        state.grid.showError(error)
    })
}