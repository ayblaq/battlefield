import Game from './game.js'
export default class Grid extends Game {
    constructor(section, arena, width, height) {
        super()
        this.width = width
        this.height = height
        this.section = section
        this.arena = arena
        this.elements = []
    }
    setUpCanvas = (canvas_id,player,arena) => {
    
        $(this.arena).append(`
            <div class="row justify-content-center canvas-container">
                <canvas id="${canvas_id}" data-attr="${player}" class="canvas" width="600" height="600"></canvas>
            </div>
        `)

        if(arena==undefined){
            $(this.arena).append(`
                <div class="row justify-content-center mb-3">
                    <div class="ship-container d-flex" id="ship-container"></div>
                </div>
                <div class="row justify-content-center mb-3">
                    <div class="col col-auto">
                        <button class="btn btn-success px-5" id="btn-playgame" disabled><i class="fas fa-play"></i>  PLAY GAME</button>
                    </div>
                </div>
            `)
        }
        
        let canvas = document.getElementById(`${canvas_id}`)
        let ctx = canvas.getContext('2d')
        ctx.fillStyle = "white"
        ctx.stroke();
        for (let i = 0; i < this.height; i++) {
            if(arena==undefined){
                $('#ship-container').append(`
                <div class="ship-box d-flex align-items-center justify-content-center" ship-attr="${i}">
                    <i class="fas fa-ship boat" draggable="true" id="ship${i}"></i>
                </div>`);
            }
            for (let j = 0; j < this.width; j++) {
                let pos = { 'left': i * 60, 'top': j * 60, 'width': 55, 'height': 55 }
                ctx.fillRect(pos.top, pos.left, pos.width, pos.height);
                if(arena==undefined)this.elements.push(pos)
            }
        }
        if(arena==undefined){
            $(this.section).removeClass("hidden")
            $(".boat")
                .on("dragstart", this.setUpDrag.start)
            $(".canvas")
                .on("dragenter", this.setUpDrag.enter)
                .on("dragover", this.setUpDrag.over)
                .on("dragleave", this.setUpDrag.leave)
                .on("drop", this.setUpDrag.drop);
        }else {
            this.data[`${player}`].canvas = canvas
        }
    }

    setUpArena = () => {
        $(this.section).empty()
        $(this.section).append(`
            <div class="row justify-content-center mb-3">
                <div class="grid-container" id="grid-container"></div>
            </div>
            <div class="row justify-content-center mb-3">
                <div class="ship-container d-flex" id="ship-container"></div>
            </div>
        `)
        let index = 0
        for (let i = 0; i < this.height; i++) {
            $('#ship-container').append(`
            <div class="ship-box d-flex align-items-center justify-content-center" ship-attr="${i}">
                <i class="fas fa-ship boat" draggable="true" id="ship${i}"></i>
            </div>`);
            for (let j = 0; j < this.width; j++) {
                index += 1
                $('#grid-container').append(`<div class="grid-box" id="grid${index}"></div>`);
            }
            $('#grid-container').append('<br>');
        }
        $(this.section).removeClass("hidden")
        $(".boat")
            .on("dragstart", this.setUpDrag.start)
        $(".grid-box")
            .on("dragenter", this.setUpDrag.enter)
            .on("dragover", this.setUpDrag.over)
            .on("dragleave", this.setUpDrag.leave)
            .on("drop", this.setUpDrag.drop);
    }

    setUpDrag = {
        start: (ev) => {
            ev.originalEvent.dataTransfer.setData("elem", ev.target.id);
        },
        enter: (ev) => {
            ev.preventDefault()
            if ($(ev.target).hasClass("grid-box")) {
                $(ev.target).addClass("dragover")
            }
        },
        over: (ev) => {
            ev.preventDefault()
            if ($(ev.target).hasClass("grid-box")) {
                $(ev.target).addClass("dragover")
            }
        },
        leave: (ev) => {
            ev.preventDefault()
            $(ev.target).removeClass("dragover")
        },
        drop: (ev) => {
            ev.preventDefault()
            if (!$(ev.target).hasClass('canvas')) {
                return false
            }
            let id = ev.originalEvent.dataTransfer.getData("elem")
            let data = document.getElementById(`${id}`)
            let canvas = document.getElementById(`${ev.target.id}`)
            let pos = this.getInterSection({'X':ev.pageX,'Y':ev.pageY},canvas,Object.values(this.data['player'].ships))
            if(pos==null){
                pos = this.getInterSection({'X':ev.pageX,'Y':ev.pageY}, canvas,this.elements)
                if(pos!==null){
                    pos['ship'] = id
                    pos['status'] = ""
                    this.data.player.ships[id] = pos
                    $(data).remove()
                    const character = window.getComputedStyle(document.querySelector('.fa-ship'), ':before').getPropertyValue(
                        'content').replace(/\"/g, "");
                    let icon = {
                        'color':"#231F20",
                        'content':character
                    }
                    this.setIconToGrid(pos.top+pos.height-10,pos.left,canvas,icon)
                }
            }
            if(Object.keys(this.data['player'].ships).length==10){
                $("#btn-playgame").attr("disabled",false)
            }
        },
    }

    gridShoot = (point,id) => {
        let canvas = this.data[`${id}`].canvas
        let x = point.X - canvas.offsetLeft;
        let y = point.Y - canvas.offsetTop;
        let opponent = this.getOpponent(id)
        let found = null

        Object.values(this.data[`${id}`]['ships']).forEach((pos) => {
            if (y >= pos.top && y < pos.top + pos.height && x >= pos.left && x < pos.left + pos.width) {
                if(pos['status']=="")pos['status'] = 'hit'
                else if(pos['status'] != "")pos['status']="replay"
                found = pos
                return true
             }
        })
        
        if(found){
            if(found.status=="hit"){
                this.data[`${opponent}`]['hit']+=1
                this.data[`${opponent}`]['turn']+=1
                let icon = {
                    'color':"red",
                    'content':"\uF05E"
                }
                this.shakeCanvas(canvas)
                this.setIconToGrid(found.top+found.height-8,found.left,canvas,this.getBoat())
                this.setIcon(found,id,'hit')
                return found;
            }
        }else {
            found = this.getInterSection(point,canvas,this.data[`${opponent}`]['miss'])
            if(found==null){
                found = this.getInterSection(point,canvas,this.elements)
                if(found){
                    this.data[`${opponent}`]['miss'].push(found)
                    this.data[`${opponent}`]['turn']+=1
                    let icon = {
                        'color':"#231F20",
                        'content':"\uF192"
                    }
                    this.setIcon(found,id,'miss')
                    return found;
                }
            }
        }
        return false;
    }

    setIconToGrid = (top, left,canvas,icon) => {
        var ctx = canvas.getContext("2d");
        document.fonts.ready.then(_ => {
            ctx.font = '900 45px "Font Awesome 5 Free"';
            ctx.fillStyle = `${icon.color}`
            ctx.globalCompositeOperation = "source-atop";
            setTimeout(_ => ctx.fillText(icon.content, left, top), 200);
        });
    }

    getInterSection = (point,canvas,array_elem) => {
        let x = point.X - canvas.offsetLeft;
        let y = point.Y - canvas.offsetTop;
        let elem = null
        array_elem.forEach((pos) => {
            if (y >= pos.top && y < pos.top + pos.height && x >= pos.left && x < pos.left + pos.width) {
                elem = Object.assign({},pos)
                return true
             }
        })
        return elem;
    }

    shakeCanvas = (canvas) => {
        let audio = document.getElementById("audio");
        audio.play();
        $(canvas).addClass('shake')
        setTimeout(()=> {
            $(canvas).removeClass('shake')
        },500)
    }
}


