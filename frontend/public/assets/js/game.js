import { API_URL } from '../js/config.js';
export default class Game {
    constructor() {
        this.data = {
            'k1': {
                'miss': [],
                'hit': 0,
                'ships': {},
                'turn': 0,
                'canvas': null,
                'nickname': 'k1',
            },
            'player': {
                'miss': [],
                'hit': 0,
                'ships': {},
                'turn': 0,
                'canvas': null
            }
        }
        this.status = {
            hit: {
                icon: "far fa-dot-circle mauve",
                id: "-hit"
            },
            miss: {
                icon: "fas fa-bomb error",
                id: "-miss"
            },
            turn: {
                icon: "far fa-hand-pointer mauve",
                id: "-turn"
            },
            damage: {
                icon: "far fa-frown mauve",
                id: "-damage",
            }
        }
        this.gameover = false;
        this.api = API_URL;
    }

    setUpGamestatus = (id, playername) => {
        $(`${id}`).append(
            `<div class="player-status pt-5">
                <div class="player-name">
                    <h5>${playername}</h5>
                </div>
                <div class="tagscloud" id="player-cloud">
                </div>
            </div>
            <div class="k1-status pt-5">
                <div class="player-name">
                    <h5>K1 - Computer </h5>
                </div>
                <div class="tagscloud" id="k1-cloud">
                </div>
            </div>
            `
        )
        this.updateStatus("k1")
        this.updateStatus("player")
        $(`${id}`).removeClass('hidden')
    }

    getOpponent = (player) => {
        return player == "player" ? "k1" : "player"
    }

    updateStatus = (player) => {
        let self = this
        $(`#${player}-cloud`).empty()
        let opponent = this.getOpponent(player)
        Object.keys(this.status).map((key, index) => {
            let value = 0
            switch (true) {
                case key == 'turn':
                    value = this.data[player].turn
                    break;
                case key == 'miss':
                    value = Object.keys(this.data[player].miss).length
                    break;
                case key == 'hit':
                    value = this.data[player].hit
                    break;
                case key == 'damage':
                    value = this.data[opponent].hit
                    break;
                default:
                    value = 0;
            }
            $(`#${player}-cloud`).append(`<a href="#" class="tag-cloud-${key}" id="${player}${self.status[key].id}" ><i class="${self.status[key].icon}"></i> ${key} - ${value}</a>`)
        });
    }

    toggleModal = {
        hide: () => {
            $('#modal').hide()
            $("#modal").removeClass("in");
            $(".modal-backdrop").remove();
            $("#modal-body").empty()
        },
        show: (message, title, allowclose) => {
            $("#modal-body").empty()
            $("#modaltitle").empty()
            let element = null
            switch (true) {
                case title == "loading":
                    element = `<div class="d-flex align-items-center">
                                <strong>${message}...</strong>
                                <div class="spinner-border ml-auto white" role="status" aria-hidden="true"></div>
                            </div>`
                    break;
                case title == "thinking":
                    element = `<div class="d-flex align-items-center">
                                    <strong>K1 is thinking...</strong>
                                    <div class="spinner-grow text-success ml-auto" role="status" aria-hidden="true"></div>
                                </div>`
                    break;
                default:
                    element = `<p>${message}</p>`
            }
            if (allowclose) {
                $("#close-modal").show()
            } else $("#close-modal").hide()
            $("#modal-body").append(`${element}`)
            $("#modaltitle").append(`${title}`)
            $("#modal").addClass("in");
            $('#modal').show()
            $("#modal").modal({
                show: true
            })
        }
    }

    toggleCanvas = {
        hide: (id) => {
            let opponent = this.getOpponent(id)
            $(`canvas[id=${id}]`).addClass("hidden")
            $(`canvas[id=${opponent}]`).removeClass("hidden")
        },
        show: (id) => {
            let opponent = this.getOpponent(id)
            $(`canvas[id=${id}]`).removeClass("hidden")
            $(`canvas[id=${opponent}]`).addClass("hidden")
        }
    }

    getGameId = async (nickname) => {
        let data = new FormData();
        data.append("nickname", nickname)
        const res = await fetch(`${this.api}/game`, {
            method: 'post',
            body: data
        })
        return res;
    }

    getTurnHistory = async (gamecode) => {
        const res = await fetch(`${this.api}/${gamecode}/game`, {
            method: 'GET',
        })
        return res;
    }

    setCanvas = async (canvas, gamecode) => {
        let data = new FormData();
        data.append("canvas", canvas)
        data.append("location", JSON.stringify(this.data['player'].ships))
        const res = await fetch(`${this.api}/${gamecode}/canvas`, {
            method: 'post',
            body: data,
            headers: new Headers({
                'Authorization': `Bearer ${this.data['player'].token}`
            })
        })
        return res;
    }

    markApi = async (player, point, gamecode) => {
        let data = new FormData();
        data.append("point", JSON.stringify(point))
        const res = await fetch(`${this.api}/${gamecode}/markpoint`, {
            method: 'post',
            body: data,
            headers: new Headers({
                'Authorization': `Bearer ${this.data[player].token}`
            })
        })
        return res;
    }

    showError = (error) => {
        if (error instanceof Error) {
            error.text().then(errorMessage => {
                let result = JSON.parse(errorMessage)
                let eStr = "";
                for (let val in result) {
                    let errors = result[val]
                    if ((typeof errors) === "string") {
                        eStr = errors;
                    } else {
                        eStr = [];
                        for (let key in errors) {
                            if (errors.hasOwnProperty(key)) {
                                eStr.push(errors[key]);
                            }
                        }
                        eStr = eStr.join("<br/>");
                    }
                }
                this.toggleModal.show(eStr, 'error', true)
            })
        } else {
            this.toggleModal.show(error, 'error', true)
        }
    }

    markPoint = (player, point, gamecode) => new Promise((resolve, reject) => {
        let opponent = this.getOpponent(player)
        this.markApi(player, point, gamecode)
            .then(response => {
                if (!response.ok) throw response
                return response.json()
            })
            .then(result => {
                this.toggleModal.hide()
                this.data[opponent].ships = result.ships
                this.data[player].miss = result.miss
                this.data[player].hit = result.hit
                this.gameover = result.gameover
                this.updateStatus(player)
                this.updateStatus(opponent)
                if (result.gameover) {
                    this.endGame(player, gamecode)
                    resolve({ 'clicked': true })
                } else {
                    setTimeout(() => {
                        this.toggleCanvas.show("player")
                        this.toggleModal.show("K1 is thinking", "thinking")
                        point = { 'X': 0, 'Y': 0 }
                        this.markK1('k1', point, gamecode)
                            .then((result) => {
                                resolve(result)
                            })
                    }, 2000)
                }
            })
            .catch(error => {
                reject(error)
            })
    })

    markK1 = (player, point, gamecode) => new Promise((resolve, reject) => {
        let opponent = this.getOpponent(player)
        this.markApi(player, point, gamecode)
            .then(response => {
                if (!response.ok) throw response
                return response.json()
            })
            .then(result => {
                this.data[opponent].ships = result.ships
                this.data[player].miss = result.miss
                let prevhit = this.data[player].hit
                let status = prevhit == result.hit ? 'miss' : 'hit'
                this.data[player].hit = result.hit
                this.data[player].turn = result.miss.length + result.hit
                this.updateStatus(player)
                this.updateStatus(opponent)
                this.setIcon(result.position, opponent, status)
                this.toggleModal.hide()
                this.gameover = result.gameover
                if (this.gameover) {
                    this.endGame(player, gamecode)
                    resolve({ 'clicked': true })
                } else {
                    setTimeout(() => {
                        this.toggleCanvas.hide("player")
                        resolve({ 'clicked': false })
                    }, 2000)
                }
            })
            .catch(error => {
                reject(error)
            })
    })

    endGame = (player, gamecode) => {
        let opponent = this.getOpponent(player)
        this.toggleModal.show(`Game Over. ${this.data[player].nickname} won the game.`, "Game Over", true)
        let canvas = document.getElementById(`${player}`)
        Object.keys(this.data[player].ships).forEach((index) => {
            let pos = this.data[player].ships[index]
            if (pos.status !== "hit") {
                this.setIconToGrid(pos.top + pos.height - 10, pos.left, canvas, this.getBoat())
            }
        })
        $(`#${opponent}-cloud`).append(`
            <div class="tag-cloud-canvas active" data-canvas="${opponent}"><i class="far fa-eye mauve"></i> View Canvas</div>
        `)
        $(`#${player}-cloud`).append(`
            <div class="tag-cloud-canvas" data-canvas="${player}"><i class="far fa-eye mauve"></i> View Canvas</div>
        `)
        $('#game-section').append(`
            <div class="row justify-content-center mb-3 mt-3">
                <div class="col col-auto">
                    <button class="btn btn-success px-5" id="btn-restart"><i class="fas fa-sync-alt"></i>  RESTART GAME</button>
                </div>
            </div>
        `)

        this.getTurnHistory(gamecode)
            .then(response => {
                if (!response.ok) throw response
                return response.json()
            })
            .then(result => {
                this.data['history'] = result
            })
            .catch(error => {
                this.showError(error);
            })
    }

    setIcon = (pos, player, type) => {
        let icon = {}
        let top = pos.top + pos.height - 10
        let left = pos.left + 5
        let canvas = document.getElementById(`${player}`)
        switch (true) {
            case type === "miss":
                icon = {
                    'color': "#231F20",
                    'content': "\uF192"
                }
                break;
            case type === "hit":
                icon = {
                    'color': "red",
                    'content': "\uF05E"
                }
                this.shakeCanvas(canvas)
                this.setIconToGrid(pos.top + pos.height - 10, pos.left, canvas, this.getBoat())
                break;
        }
        let ctx = canvas.getContext("2d");
        document.fonts.ready.then(_ => {
            ctx.font = '900 45px "Font Awesome 5 Free"';
            ctx.strokeStyle = `${icon.color}`
            setTimeout(_ => ctx.strokeText(icon.content, left, top), 200);
        });
    }

    getBoat = () => {
        const character = window.getComputedStyle(document.querySelector('.fa-ship'), ':before').getPropertyValue(
            'content').replace(/\"/g, "");
        let icon = {
            'color': "#231F20",
            'content': character
        }
        return icon
    }
}
